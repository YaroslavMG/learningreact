import React from 'react'

const withSlass = (Component, className) => {
    return props => {
        return (
            <div className={className}>
                <Component {...props} />
            </div>
        )
    }
}

export default withSlass